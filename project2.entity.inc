<?php
/**
 * @file
 */
 
/**
 * The class used for project entities.
 */
class Project extends Entity {

  /**
   * The project id.
   *
   * @var integer
   */
  public $project_id;

  /**
   * The name of the project type.
   *
   * @var string
   */
  public $type;

  /**
   * The project label.
   *
   * @var string
   */
  public $title;
  
  /**
   * The project state.
   *
   * @var string
   */
  public $state;

  /**
   * The user id of the project owner.
   *
   * @var integer
   */
  public $user;

  /**
   * The Unix timestamp when the project was created.
   *
   * @var integer
   */
  public $created;

  /**
   * The Unix timestamp when the project was most recently saved.
   *
   * @var integer
   */
  public $changed;


  public function __construct($values = array()) {
    parent::__construct($values, 'project2');
  }

  public function defaultUri() {
    return array(
      'path' => 'project/' . $this->project_id,
      'options' => array(''),
    );
  }

  public function defaultLabel() {
    return $this->title;
  }


  public function save() {
    // Care about setting created and changed values. But do not automatically
    // set a created values for already existing projects.
    if (empty($this->created) && (!empty($this->is_new) || !$this->project_id)) {
      $this->created = REQUEST_TIME;
    }
    $this->changed = REQUEST_TIME;

    parent::save();
  }
}