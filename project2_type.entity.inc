<?php
/**
 * @file
 */

/**
 * Use a separate class for project types so we can specify some defaults
 * modules may alter.
 */
class ProjectType extends Entity {

  /**
   * Machine name of the Project type.
   *
   * @var string
   */
  public $type;
  
  /**
   * Human readable name of the Project type.
   *
   * @var string
   */
  public $label;
  
  /**
   * An array of possible states this project can be in of the form
   * $state_machine_name => $state label
   *
   * @var array
   */
  public $states = array();

  public function __construct($values = array()) {
    parent::__construct($values, 'project2_type');
  }

  /**
   * Returns whether the project type is locked, thus may not be deleted or renamed.
   *
   * Project types provided in code are automatically treated as locked, as well
   * as any fixed project type.
   */
  public function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}