<?php

/**
 * @file
 * Project type editing UI.
 */

/**
 * UI controller.
 */
class Project2TypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage projects, including fields.';
    return $items;
  }
}

/**
 * Generates the project type editing form.
 */
function project2_type_form($form, &$form_state, $project_type, $op = 'edit') {

  if ($op == 'clone') {
    $project_type->label .= ' (cloned)';
    $project_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $project_type->label,
    '#description' => t('The human-readable name of this project type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($project_type->type) ? $project_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $project_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'project2_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this project type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $states_string = '';
  foreach ($project_type->states as $state_name => $state_label) {
    $states_string += $state_name.'|'.$state_label."\n";
  }
  
  $form['states'] = array(
    '#type' => 'textarea',
    '#default_value' => $states_string,
    '#title' => t('States'),
    '#description' => t('The states (statuses) this project goes through'),
  );
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save project type'),
    '#weight' => 40,
  );

  if (!$project_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete project type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('project2_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function project2_type_form_submit(&$form, &$form_state) {
  // Build the project type
  $project_type = entity_ui_form_submit_build_entity($form, $form_state);
  
  $project_type->states = explode('\n', $project_type->states);
  foreach($project_type->states as $key => $var) {
    unset($project_type->states[$key]);
    list($name, $label) = explode('|', $var);
    $project_type->states[$name] = $label;
  }  
  
  // Save and go back.
  $project_type->save();
  $form_state['redirect'] = 'admin/structure/projects';
}

/**
 * Form API submit callback for the delete button.
 */
function project2_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/projects/manage/' . $form_state['project2_type']->type . '/delete';
}
