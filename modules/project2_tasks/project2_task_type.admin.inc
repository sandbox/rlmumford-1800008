<?php

/**
 * @file
 * Party Activity Type editing UI
 */

/**
 * UI Controller
 */
class ProjectTaskTypeUIController extends EntityDefaultUIController {

}

/**
 * Generates the party activty type editing form.
 */
function project2_task_type_form($form, &$form_state, $task_type, $op = 'edit') {

  if ($op == 'clone') {
    $task_type->label .= ' (cloned)';
    $task_type->type = '';
  }

  $form_state['task_type'] = $task_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $task_type->label,
    '#description' => t('The human-readable name of this activity type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($task_type->type) ? $task_type->type : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'project2_task_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this project task type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function project2_task_type_form_submit(&$form, &$form_state) {
  $task_type = entity_ui_form_submit_build_entity($form, $form_state);
  $task_type->save();
  $form_state['redirect'] = 'admin/structure/task_types';
}

/**
 * Form API submit callback for the delete button.
 */
function project2_task_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/task_types/manage/' . $form_state['task_type']->type . '/delete';
}
