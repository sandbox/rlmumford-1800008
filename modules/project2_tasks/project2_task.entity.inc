<?php
/**
 * @file Entity and Controller classes for the Task entity
 */

/**
 * The class used for Task Entities
 */
class ProjectTask extends Entity {

  public function __construct($values = array()) {
    parent::__construct($values, 'project2_task');
  }

  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return array('path' => 'task/' . $this->task_id);
  }

}

/**
 * The controller class for Party Activities
 */
class ProjectTaskController extends EntityAPIController {

  /**
   * Create a ProjectTask - we first set up the values that are specific to
   * our project2_task schema
   */
  public function create(array $values = array()) {
    $values += array(
      'task_id' => '',
      'is_new' => TRUE,
      'title' => '',
      'created' => mktime(),
      'modified' => mktime(),
    );

    return parent::create($values);
  }
}
