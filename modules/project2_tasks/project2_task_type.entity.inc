<?php
/**
 * @file Entity and Controller classes for the Project Task Type entities
 */

/**
 * Entity class for project task types
 */
class ProjectTaskType extends Entity {
  public $type;
  public $label;

  public function __construct($values = array()) {
    parent::__construct($values, 'project2_task_type');
  }
}
